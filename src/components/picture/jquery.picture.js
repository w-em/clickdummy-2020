;(function ($, window) {
  'use strict';

  $.plugin('wemPicture', {
    defaults: {
      containerSelector: '[data-picture]',
      lastLoadedBreakpoint: null,
      placeholder: 'data:image/gif;base64,R0lGODlhEAAQAOZtAOzt7q2xte3u7tnc3dve4Njb3bi8v6+zt+3t7trd36+ytq6ytrC0uLG1ue7u7+3u7+fo6bK1ube6vrW4vNba2+Tl5uXm5+Lk5czO0Ly/w8jKzdfZ27q9wd7g4eHi5NbZ2tve39zg4b7CxcXIy7a5vNTW2d3f4b3Bw7S4u6uwtN/h4ri7v+nq69ja3Nzf4O/w8by+wtHT1sLGybK2uayvs7G1uLe6vdjc3tTY2N/i49XZ266xtqywta2xtM7R07G2ubu+wejp6rO2urG0uKyxtNnb3ayxtcDDxvT09a2wtNvd37W5vrCzt6+yt+rq66+0uNja3fX19ujp6a+0t7q+wtjb3K6xtayws/Lz86+zttna3bC1ubi7wLC1uOrs7a6zt7u9wuzu79ra3ejo6vDx8bS3u+zs7dfa3NbZ2+vs7dXY2urr7KywtP///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFBABtACwAAAAAEAAQAAAHPoAiBoOEBk08iIk8ZWyNjmwpj46MkpCVbJSSkZWZj5uSnY6fj6GNo5OXp42llpypl6yqmK+ulbKxsLSguo+BACH5BAUEAG0ALAAAAAAQABAAAAc7gAaCg4JDQgqIiWyLjIs7iZCNjY+QiJKMlJWXjpWWm5mRn50Km2ygiqKdpaeel6ykqZqxoa6jq7aziYEAIfkEBQQAbQAsCAAAAAgAEAAAByaAHA2DhCI2hIQnh4gNioyNi4iOjJOSkYmXg5WYj5uamZCdoJ6NgQAh+QQFBABtACwIAAAACAAQAAAHKoBtZYODBAQGhINqaoiJi42Ej4llko6Mk5WRl5aQipuanZSfnqGZpJhqgQAh+QQFBABtACwIAAAABgAQAAAHJIAZEhJHBASCEiJqaoiKjIOOjYuSj4mTkJeWlZGYm5mcmpSUgQAh+QQFBABtACwIAAAABAAQAAAHGIAnBIMwaoaFh4ZqiIuKjI+OkYmTjZSMgQAh+QQFBABtACwHAAAAAgAQAAAHDoAuJS5qhIaFiIeKiYyBACH5BAUEAG0ALAUAAAAEABAAAAcYgASCVYIEA4WHgomGiI2KjoyPkpGUiwOBACH5BAUEAG0ALAMAAAAGABAAAAcggBQJgwlFH4QJN4eEioiNjIuDj5KRiZWTlo6Xm5qdjIEAIfkEBQQAbQAsAgAAAAcAEAAABx6AA4KDUIODBYaCiImLho2HiQOPipGTkpWYjJmOlYEAIfkEBQQAbQAsAAAAAAkAEAAABy2AMh9thIRVZxw4hYUtiYuEjYqPkY9tlJOOmJKLl5yZnpuMn6KhkKOmpZanloEAIfkEBQQAbQAsAAAAAAkAEAAAByeARQWDhAVnaoWEh4mDi4yOiZCFkoqIj5aRmJOalYyGnI2gn56UjYEAIfkEBQQAbQAsAAAAAAkAEAAABySAJmeDhC0UBISJZ4iKg4yNj4qRiZOElY6NmJmXi5mdm56cnIEAIfkEBQQAbQAsAAAAAAkAEAAABySAbYKDBQMFJoOJbYiKgoyNj4qRiZODlY6NmJmXi5mdm56cnIEAIfkEBQQAbQAsAAAAAAkAEAAAByyALmiDbS1tbVAgg4uDFIqMjY+QjpCRlWiUlZmTkoybnp2Ln6KhlpqlmKgUgQAh+QQFBABtACwAAAAACAAQAAAHJoAmG2dVBQMDCSFti4xtio2Lj5CSjZSMlpGQmZqYjpqenJ+do5+BACH5BAUEAG0ALAAAAAAIABAAAAcjgB4FA4SFHWiIiWiHioiMjY+KkYmTjo2Wl5WLl5uZnJqgnIEAIfkEBQQAbQAsAAAAAAcAEAAAByGAFwNtCW2EOYaJbYiKi42OjYyKkomUhpaQk4+YnJuejYEAIfkEBQQAbQAsAAAAAAkAEAAAByuAbQWDhAUbKmqJimo6iIuJjY+Qjo+RkpaVlIuYm5qKnJ+ek5KMoqWkoJCBACH5BAUEAG0ALAAAAAAQABAAAAc2gAMbg4SFhmhqiYqLjIiMj4qOkI+Sk4uVlomYmZuWaCWgoaKjBJmMpaaKqKlqq6muprCZspaBACH5BAUEAG0ALAAAAAAQAAkAAAcegAOCg4SFA2iIiYqLjI2Oj5CRkpOLZ5SIapmam5yBACH5BAUEAG0ALAAAAAAQAAkAAAcdgABrg4SFhmeIiYqLjI2Oj5CRkpOUjUUbmJmam4EAIfkEBQQAbQAsAAAAABAACQAAByeALwKDhIWGZmuJiouMHY6PkJEdBZSVlpeYmZqbnJ2elFVooqOkpYEAIfkEBQQAbQAsAAACABAABwAAByGAZmuDhIWGgoaJhC4hjY6PkAmSk5SVlpeYmZRnnJ2en4EAIfkEBQQAbQAsAAAEABAABQAABx6AZmuDhIWGgoaJhBeMjY6PFyCSk5SVIAMfmZqbnIEAIfkEBQQAbQAsAAAGABAAAwAABxKAZmuDhIWGgoaJhARnjY6PkIEAIfkEBQQAbQAsAAAIABAAAwAABxGACA6DhIWGh4iEThCMjY6PgQAh+QQFBABtACwAAAgAEAAFAAAHFIBpDoOEhYaHiImKi4kVHo+QkZKBACH5BAUEAG0ALAAACAAQAAYAAAcVgGsAg4SFhg+IiYqLjI2Oj5CRkoiBACH5BAUEAG0ALAAACAAQAAcAAAcWgE5rg4SFhgiIiYqLjI2Oj5CRkpOLgQAh+QQFBABtACwAAAgAEAAIAAAHHIAIgoOEhQgAiImKi4yNjo+QkZKTi0EQl5iZmoEAIfkEBQQAbQAsAAAIABAACAAABxWAAgCDhIWGaYaJiouMjY6PkJGSAIEAIfkEBQQAbQAsAAAIABAACAAABxiADwiDhIWGa2aJiouMjY6PkJGSk5SVjYEAIfkEBQQAbQAsAAAIABAACAAABxaAAGmDhIWGbYaJhWuKjY6PkJGSk5CBACH5BAUEAG0ALAAACAAQAAQAAAcVgAIAg4SFhm2IiYqLbSyMj4hrkI+BACH5BAUEAG0ALAAACAAQAAYAAAcXgABpg4SFhl6GiYqLhk6MiWuPhpGShIEAIfkEBQQAbQAsAAAJAAEABgAABweAayxrhGuBACH5BAUEAG0ALAAACQABAAcAAAcIgE5tLIMsa4EAIfkEBQQAbQAsAAAIAAEABwAABweAZmuDLIOBACH5BAUEAG0ALAAACAAQAAgAAAcbgGlrg4SFhmaGiYWIioqMjYeQiY+Sg5SVl5KBACH5BAUEAG0ALAAAAAAQABAAAAdQgGRmg4SFhlEPaW2LjI2MUWZpAI6UbVGKaYqVj4ySm4tIZp2Tm0iai56VSJSpjqGsYZSmla2gn7Wrt6RtuZ8ApFifnV5twcKoAFinqI6KAIEAIfkEBQQAbQAsAAAAABAAEAAAB2KAG0FthIWGhhgWEIeMhBoWFYuNhiMWbRWWk4QykpiabTIXhZCaRx6GFpmMHKKoqoZAp4epjBkmjbSGBre4FYZlRZoVvoQSG5+ebSvHyJYoJZ+EmGUjRR4eHReYqRYsYxAVgQAh+QQFBABtACwAAAAADwAQAAAHbYBnEG2EhYaFGm0Wh4xtIhUWi42FHB1pFhWThGUbaZeZk2UYnmltF6EjLKSmjWUZEKSlp4dlKBWxpaCFZWwmuLKGvCW/pR6GbKPEFrO8IkHEl5tsK6fQmLxsJxoxRRcQqqSLbOPkBwYyGCUmmYEAIfkEBQQAbQAsAAAAABAAEAAAB16ALW2DhIWGHBCGioRlJomLhmUybY+QhAcQAJWWbGcAmpaDbBmfoJwLmZ9SnGw+paaLbBKvsIZsbBe0q4q3GrQAu4W3bDGppZttw7dAZ0HHtspsBzIeLLXCymUaFRaBACH5BAUEAG0ALAAAAQAPAA8AAAdFgAZtg4SFhiSGiYRsG4qJbGwIjoZsK5KTg5AVl5OQkQgQmJBHCJyKnkGlopBOpaGnnhqujp5sNKlSnZ4iprCQJpiLbAaBACH5BAUEAG0ALAAAAAAQABAAAAc9gDFtg4SFhm0Yh4qDM4mLh2WOj4WRk4VsEz6WhJiSlmwkmptsoKKfoJ6PpKGbbaQoqYukma2uQrGPlbWNgQAh+QQFBABtACwAAAAAEAAQAAAHQIAabYOEhYZtGReHi4MZGIyLGQYWkIaSI5WFJwYGEJmNnCKfbZsGGaOSBoqfqaOIBqKjQKquMAaubTCYtR64GYEAIfkEBQQAbQAsAQABAA8ADwAAB02AHBoWbYWGh4ZAKzJFiI5tVBISGT5Bj4aRkgYjhJcZkqAyLJdAoJIel22foCKpbaWSKxCusBIaroWalq4ckiW4qpOjuL2owEAywIVggQAh+QQFBABtACwBAAAADwAQAAAHVYAda22EhYaFBicaHxWHjisTEytHPiosjoSQkRMSGSMlF4+boxwmjgajmxiYqKkZQaypKxCYba2RJEq1tqMal7W3J7C7miQqu5mRPsjJIsyZXM+EXIEAIfkEBQQAbQAsAAAAABAAEAAAB26AGTFKKhWGh4iGTRMGJyMYJQMXLGuVlmxsKGWbJAZURyM+BSqUmJmbqKhLVEVrpj2aqagkMa6mAbGyZRospphWualHQb6YuLpAEMXGwWUSJsvMqUtq0dKbKBjW17vb10femEmaBuGYRihL5qYogQAh+QQFBABtACwAAAAAEAAQAAAHXIBAGCViHoZtiImKZRMGHBlHIxgxJUUeQRAsiGURnZ5CEyQkKzIViZyeqZ0TEIuqqWUgim2or2Uxs7SvEWUYubqvGr/AqSPDxJ1Hx8gcy8QrzsDQ0Zwk0Ztl15uBACH5BAUEAG0ALAEAAAAPABAAAAdlgBgxalpaRR4qHR4VFReMQhNLMyscBhwiRzIjIxhKKT8MoaKjDQwybJ+jqgwNQGxsRKCroiKvsLKzJ7ZsPbiqHLu3sxLBwqrExbGqJMWvyqLMzcYMKNLOskLWtqDZ2rcz3q8pM4EAIfkEBQQAbQAsAAAAABAAEAAAB2SAHBoYMYUlh2pKSlpKHiZtEWVlDSQTMyQrEzYcBhwibWw1B6OkpaNLbaCipqYkqW09q6ynr7CyrEK1qbelM7q2s2W/wKa+w7GmDcOpyKTGy80HEcuvzQzUr0miytivNdzdoA2BACH5BAUEAG0ALAAAAAAQABAAAAddgBwaGBg+hYYxiYklMQs1DTUHZTOSZZU1MytsNAwLnp+gngxsm52hoaOkbKannkOqbDysp12wbEmzoAe2sbmeW7y9tMHCusTFng3HyMDLsp67y6ULqdIpDEzSqpyBACH5BAUEAG0ALAAAAAAQABAAAAdYgBwaGISFhoUaTUMMDAsRDAENkAwNAQxCbCkHAZydnpwHbJmbn6WibESkpaCnbEaqpaGtPbCfraKvq7K3uZ8Mt6e9nsDBsLvEwsfIpFbEwJvKzq/RzilTgQAh+QQFBABtACwAAAAAEAAQAAAHRYBtIxptbRqEgxojiSMLbUNPj5GQhZFMQ200hZucnYWanqGFV6KibKWhSaiep6ucrZ0Hpq6vtJuwbQyrsAGuoLanjrSagQAh+QQFBABtACwBAAAADwAMAAAHLYAabYODgoMybSMyiG0MAQ0Mg0xtT5Ftk4SZmm0Lm56foKGio6KdpKeonKcHgQAh+QQFBABtACwBAAAADwAQAAAHTIAjbYOChCOFg0wMU2wMB41ZbF8LbFkHbZRsmpucbAGDmZ2dg5iinJ+koaILpKCmmq2uorGpq7SynLe4nrq4rL2lsMClv8MLqMNtWYEAIfkEBQQAbQAsAAAAABAAEAAABy+AIgaDhIWGZWyJiouMiIyPio6Qj5KTi5WWiZiZm5adk5+QoZSZjaWXp5GpmqtsgQAh+QQFBABtACwAAAAAAQABAAAHA4BtgQAh+QQFBABtACwAAAAAAQABAAAHA4BtgQA7',
    },

    /**
     * Inits the plugin.
     */
    init: function () {
      var me = this;

      me.applyDataAttributes();

      me.setRanges();
      // me.setLazyLoader();

      me.registerEvents();

      me.$wrapper = $('<span class="picture__wrapper"></span>');

      me.$el.before(this.$wrapper);
      me.$clone = $('<canvas class="picture__clone"></canvas>');

      me.$wrapper.append(me.$clone);
      me.$wrapper.append(me.$el);

      me.load();
    },

    load () {
      var me = this

      if (me.opts.lastLoadedBreakpoint === StateManager.getCurrentState()) {
        return false;
      }

      var newImage = me.getImageSrcMobileFirst();
      var currentImage = me.$el.attr('src');

      if (newImage === currentImage)  {
        return false;
      }

      this.activateLoader()

      this.updateCanvasSize();

      var tmpImg = new Image();

      tmpImg.onload = function () {
        me.$el.attr('src', newImage)
        me.deactivateLoader();
        me.opts.lastLoadedBreakpoint = StateManager.getCurrentState();
        me.$el.show();
      };
      tmpImg.src = newImage;
    },

    /**
     * acativate the loading bar
     * @public
     * @alias module:Picture.getImageSrcMobileFirst
     * @returns void
     */
    activateLoader () {
      this.$clone.css('background-image', "url('" + this.opts.placeholder + "')");
      this.$wrapper.show();
      this.$clone.show();
    },

    deactivateLoader () {
      this.$clone.hide();
      this.$el.show();
    },

    /**
     * Get the source path from DOM object if empty
     * test for the next lower breakpoint
     * @public
     * @alias module:Picture.getImageSrcMobileFirst
     * @returns {string|null}
     */
    getImageSrcMobileFirst: function getImageSrc () {

      var src = null

      var breakpoints = this.breakpoints

      for (var i = 0; i < breakpoints.length; i++) {
        var breakpoint = breakpoints[i];
        var breakpointSource = this.getImageSrc(breakpoint.state)

        if (breakpointSource !== null) {
          src = breakpointSource
        }

        if (breakpoint.state === StateManager.getCurrentState()) {
          return src
        }
      }
      return src;
    },

    /**
     * Get the source path from DOM object
     * @public
     * @alias module:Picture.getImageSrc
     * @param breakingpoint
     * @returns {string|null}
     */
    getImageSrc: function getImageSrc (breakingpoint) {
      // Get source from data attribute
      var src = this.$el.attr('data-picture-' + breakingpoint + '-src') || null;

      return src;
    },

    /**
     * Returns default width
     * @public
     * @alias module:Picture.getImageHeightMobileFirst
     * @returns {number}
     */
    getImageHeightMobileFirst: function getImageHeightMobileFirst () {

      var height = this.getImageHeight(StateManager.getCurrentState());

      var breakpoints = this.breakpoints

      for (var i = 0; i < breakpoints.length; i++) {
        var breakpoint = breakpoints[i];
        var breakpointSource = this.getImageHeight(breakpoint.state)

        if (breakpointSource !== null) {
          height = breakpointSource
        }

        if (breakpoint.state === StateManager.getCurrentState()) {
          return height
        }
      }

      if (height === null) {
        return '100px'
      }

      return height;
    },

    /**
     * Get the source width from DOM object
     * @public
     * @alias module:Picture.getImageHeight
     * @param breakingpoint
     * @returns {number}
     */
    getImageHeight: function (breakingpoint) {
      return this.$el.data('picture-' + breakingpoint + '-height') || null;
    },

    /**
     * Returns default width
     * @public
     * @alias module:Picture.getImageWidthMobileFirst
     * @returns {number}
     */
    getImageWidthMobileFirst: function getImageWidthMobileFirst () {

      var width = this.getImageWidth(StateManager.getCurrentState());

      var breakpoints = this.breakpoints

      for (var i = 0; i < breakpoints.length; i++) {
        var breakpoint = breakpoints[i];
        var breakpointSource = this.getImageWidth(breakpoint.state)

        if (breakpointSource !== null) {
          width = breakpointSource
        }

        if (breakpoint.state === StateManager.getCurrentState()) {
          return width
        }
      }

      if (width === null) {
        return '100%'
      }

      return width;
    },

    /**
     * Get the source width from DOM object
     * @public
     * @alias module:Picture.getImageWidth
     * @param breakingpoint
     * @returns {number}
     */
    getImageWidth: function (breakingpoint) {
      return this.$el.data('picture-' + breakingpoint + '-width') || null;
    },

    /**
     * @returns
     */
    getParent () {
      return this.$el.parent();
    },

    /**
     * @returns {*|jQuery|HTMLElement}
     */
    updateCanvasSize () {
      var width = this.getImageWidthMobileFirst();
      var height = this.getImageHeightMobileFirst();
      var src = this.getImageSrcMobileFirst();
      var $parent = this.getParent();
      var scaleFactor = 1;
      if ($parent) {
        var parentWidth = $parent.width();
        scaleFactor = parentWidth / width;
      }

      return this.$clone.css({
        width: width * scaleFactor,
        height: height * scaleFactor
      });
    },

    /**
     *
     * @returns {boolean|boolean}
     */
    isInViewport: function () {
      var $element = this.$el;
      var elementTop = $element.offset().top - 100;
      var elementBottom = elementTop + $element.outerHeight();

      var viewportTop = $(window).scrollTop();
      var viewportBottom = viewportTop + $(window).height();

      return elementBottom > viewportTop && elementTop < viewportBottom;
    },

    /**
     * Set responsive ranges
     * @public
     * @alias module:Picture.setRanges
     * @returns {object}
     */
    setRanges: function () {
      var breakpoints = StateManager.getBreakpoints().sort(function compare (a, b) {
        if (a.enter > b.enter) {
          return 1;
        }
        if (a.enter < b.enter) {
          return -1;
        }
        return 0;
      })
      this.breakpoints = breakpoints
      return breakpoints
    },

    /**
     * Registers all events.
     */
    registerEvents: function () {
      var me = this;

      var debounce = function (func, delay) {
        let inDebounce
        return function() {
          const context = this
          const args = arguments
          clearTimeout(inDebounce)
          inDebounce = setTimeout(function () {func.apply(context, args)}, delay)
        }
      }
      me._on($(window), 'resize', $.proxy(me.onResize, me));
      me._on($(window), 'scroll', $.proxy(me.onScroll, me));
    },

    onResize () {
      this.load();
    },

    onScroll () {
      this.load();
    }
  });
})(jQuery, window);
