;(function ($) {
  'use strict';

  /**
   * Swiper Slider Wrapper Plugin
   */
  
  $.plugin('wemSlider', {
    sliderSelector: '[data-controller="swiper/slider"]',
    sliderWrapperSelector: '[data-controller="swiper/slider/wrapper"]',
    sliderFilterButtonSelector: '[data-controller="slider/filter/button"]',
    sliderNavigationNextSelector: '[data-controller="slider/nav/next"]',
    sliderNavigationPrevSelector: '[data-controller="slider/nav/prev"]',
    filterButtonActiveClass: 'btn-secondary',
    filterButtonInActiveClass: 'btn-link',
    defaults: {
      init: true,
      direction: 'horizontal',
      touchEventsTarget: 'container',
      initialSlide: 0,
      speed: 300,
      cssMode: false,
      updateOnWindowResize: true,
      //
      preventInteractionOnTransition: false,

      // To support iOS's swipe-to-go-back gesture (when being used in-app).
      edgeSwipeDetection: false,
      edgeSwipeThreshold: 20,

      // Free mode
      freeMode: false,
      freeModeMomentum: true,
      freeModeMomentumRatio: 1,
      freeModeMomentumBounce: true,
      freeModeMomentumBounceRatio: 1,
      freeModeMomentumVelocityRatio: 1,
      freeModeSticky: false,
      freeModeMinimumVelocity: 0.02,

      // Autoheight
      autoHeight: false,

      // Set wrapper width
      setWrapperSize: false,

      // Virtual Translate
      virtualTranslate: false,

      // Effects
      effect: 'slide', // 'slide' or 'fade' or 'cube' or 'coverflow' or 'flip'

      // Breakpoints
      breakpoints: {},

      // Slides grid
      spaceBetween: 0,
      slidesPerView: 1,
      slidesPerColumn: 1,
      slidesPerColumnFill: 'column',
      slidesPerGroup: 1,
      slidesPerGroupSkip: 0,
      centeredSlides: false,
      centeredSlidesBounds: false,
      slidesOffsetBefore: 0, // in px
      slidesOffsetAfter: 0, // in px
      normalizeSlideIndex: true,
      centerInsufficientSlides: false,

      // Disable swiper and hide navigation when container not overflow
      watchOverflow: false,

      // Round length
      roundLengths: false,

      // Touches
      touchRatio: 1,
      touchAngle: 45,
      simulateTouch: true,
      shortSwipes: true,
      longSwipes: true,
      longSwipesRatio: 0.5,
      longSwipesMs: 300,
      followFinger: true,
      allowTouchMove: true,
      threshold: 0,
      touchMoveStopPropagation: false,
      touchStartPreventDefault: true,
      touchStartForcePreventDefault: false,
      touchReleaseOnEdges: false,

      // Unique Navigation Elements
      uniqueNavElements: true,

      // Resistance
      resistance: true,
      resistanceRatio: 0.85,

      // Progress
      watchSlidesProgress: false,
      watchSlidesVisibility: false,

      // Cursor
      grabCursor: false,

      // Clicks
      preventClicks: true,
      preventClicksPropagation: true,
      slideToClickedSlide: false,

      observer: true,
      observeParents: true,

      // Images
      preloadImages: true,
      updateOnImagesReady: true,
      lazy: false,

      // loop
      loop: true,
      loopAdditionalSlides: 0,
      loopedSlides: null,
      loopFillGroupWithBlank: false,

      // Swiping/no swiping
      allowSlidePrev: true,
      allowSlideNext: true,
      swipeHandler: null, // '.swipe-handler',
      noSwiping: false,
      noSwipingClass: 'swiper-no-swiping',
      noSwipingSelector: null,

      // Passive Listeners
      passiveListeners: true,
      // NS
      containerModifierClass: 'swiper-container-', // NEW
      slideClass: 'swiper-slide',
      slideBlankClass: 'swiper-slide-invisible-blank',
      slideActiveClass: 'swiper-slide-active',
      slideDuplicateActiveClass: 'swiper-slide-duplicate-active',
      slideVisibleClass: 'swiper-slide-visible',
      slideDuplicateClass: 'swiper-slide-duplicate',
      slideNextClass: 'swiper-slide-next',
      slideDuplicateNextClass: 'swiper-slide-duplicate-next',
      slidePrevClass: 'swiper-slide-prev',
      slideDuplicatePrevClass: 'swiper-slide-duplicate-prev',
      wrapperClass: 'swiper-wrapper',

      // Callbacks
      runCallbacksOnInit: true,

      mode: 'local',
      // the controller url for ajax loading.
      ajaxUrl: null,
      // the category id for ajax loading.
      ajaxCategories: [],
      // the maximum number of items to load via ajax.
      ajaxMaxShow: 10
    },

    /**
     * Method for the plugin initialisation.
     * Merges the passed options with the data attribute configurations.
     * Creates and references all needed elements and properties.
     * Calls the registerEvents method afterwards.
     *
     * @public
     * @method init
     */
    init: function () {
      var me = this;

      me.$sliderElement = me.$el.find(me.sliderSelector);
      me.$sliderWrapperElement = me.$el.find(me.sliderWrapperSelector);
      me.uuid = me.getUid();
      me.swiperContainerClass = 'swiper-slider-uuid-' + me.uuid;
      me.$sliderElement.addClass(me.swiperContainerClass);

      var breakpoints = StateManager.getBreakpoints();

      $.each(breakpoints, function (index, breakpoint) {
        me.opts[breakpoint.state] = {}
      })

      me.applyDataAttributes();
      me.convertOptions();

      me.$el.find(me.sliderFilterButtonSelector).on('click', $.proxy(me.onFilterButtonClicked, me))
      me.$el.find(me.sliderNavigationNextSelector).on('click', $.proxy(me.onClickNext, me))
      me.$el.find(me.sliderNavigationPrevSelector).on('click', $.proxy(me.onClickPrev, me))

      if (me.opts.mode === 'local') {
        me.$allItems = me.$sliderWrapperElement.children();
      }

      me.initSlider();
    },

    onClickNext: function (e) {
      var me = this;
      me.$slider.slideNext();
    },

    onClickPrev: function (e) {
      var me = this;
      me.$slider.slidePrev();
    },

    onFilterButtonClicked: function(e) {
      var me = this
      me.showPreloading();
      var $btn = $(e.target);
      $btn.parent().find('.' + me.filterButtonActiveClass).removeClass(me.filterButtonActiveClass).addClass(me.filterButtonInActiveClass);
      $btn.addClass(me.filterButtonActiveClass).removeClass(me.filterButtonInActiveClass);
      if (me.opts.mode === 'ajax') {
        me.loadItems(0, me.opts.ajaxMaxShow, parseInt($(e.target).data('value')), $.proxy(me.initSlider, me));
      }
      else if (me.opts.mode === 'local') {
        me.filterItems($(e.target).data('value'), $.proxy(me.initSlider, me));
      }

    },

    showPreloading: function () {

    },

    /**
     * Converts options from data atrributes to options the slick slider can handle.
     *
     * @public
     * @method convertOptions
     */
    convertOptions: function () {
      var me = this,
        opts = me.opts,
        breakpoints = StateManager.getBreakpoints(),
        breakpointsLength = Object.keys(StateManager.getBreakpoints());

      breakpoints.sort(function (a, b) {
        return a.enter > b.enter;
      });

      var convertedOpts = {
        lazy: parseInt(opts.lazy) > 0 ? {
          loadPrevNext: true,
          loadPrevNextAmount: 4
        } : false,
        preloadImages: parseInt(opts.preloadImages) > 0,
        loop: parseInt(opts.loop) > 0,
        loopedSlides: opts.loopedSlides ? parseInt(opts.loopedSlides) : 0,
        centeredSlides: parseInt(opts.centeredSlides) > 0,
        initialSlide: opts.initialSlide ? parseInt(opts.initialSlide) : 0,
        autoHeight: parseInt(opts.autoHeight) > 0,
        ajaxCategories: opts.ajaxCategories,
        breakpoints: {},
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        }
      }

      $.each(breakpoints, function (index, breakpoint) {
        convertedOpts.breakpoints[breakpoint.enter] = me.convertResponsiveSettings(opts[breakpoint.state])
      })
      $.extend(opts, convertedOpts);
    },

    convertResponsiveSettings (opts) {
      return {
        spaceBetween: opts.spaceBetween ? parseInt(opts.spaceBetween) : 0,
        slidesPerView: opts.slidesPerView ? parseFloat(opts.slidesPerView) : 0,
        slidesPerColumn: opts.slidesPerColumn ? parseFloat(opts.slidesPerColumn) : 1,
        slidesPerGroup: opts.slidesPerGroup ? parseFloat(opts.slidesPerGroup) : 1,
        centeredSlides: parseInt(opts.centeredSlides) > 0,
        initialSlide: opts.initialSlide ? parseInt(opts.initialSlide) : 0,
      }
    },

    /**
     * Initializes all necessary slider configs.
     *
     * @public
     * @method initSlider
     */
    initSlider: function () {
      var me = this,
        opts = me.opts;

      me.trackItems();

      if (me.itemsCount === 0 && opts.mode === 'ajax') {
        me.loadItems(0, opts.ajaxMaxShow, me.opts.ajaxCategories, $.proxy(me.initSlider, me));
        return;
      }

      new Swiper('.' + me.swiperContainerClass, me.opts);
      me.$slider = document.querySelector('.' + me.swiperContainerClass).swiper
    },

    /**
     * Loads new items via ajax.
     *
     * @public
     * @method filterItems
     * @param {Number} category
     * @param {Function} callback
     */
    filterItems: function (category = -1, callback) {
      var me = this;
      var response = '';

      me.$sliderWrapperElement.empty();

      for (var i = 0; i < me.$allItems.length; i++) {
        var el = me.$allItems[i];
        var $item = $(el);
        if (!category || category === -1) {
          me.$sliderWrapperElement.append($item);
        }
        else {
          if ($item.data('category').split(',').indexOf(category) > -1) {
            me.$sliderWrapperElement.append($item);
          }
        }

        setTimeout(function () {
          me.trackItems();

          if (typeof callback === 'function' && me.itemsCount > 0) {
            callback.call(me, response);
          }
        }, 100)

      }
    },

    /**
     * Loads new items via ajax.
     *
     * @public
     * @method loadItems
     * @param {Number} start
     * @param {Number} limit
     * @param {Number} category
     * @param {Function} callback
     */
    loadItems: function (start, limit, category = -1, callback) {
      var me = this,
        data = {
          'start': start,
          'limit': limit,
          'category': category
        };

      $.publish('plugin/slider/onLoadItemsBefore', [ me, data ]);

      if (!me.opts.ajaxUrl) {
        return false
      }

      $.ajax({
        url: me.opts.ajaxUrl,
        method: 'GET',
        data: data,
        success: function (response) {
          if (!response) {
            // Prevent infinite loop
            return;
          }
          me.$sliderWrapperElement.empty();
          me.$sliderWrapperElement.append(response);

          me.trackItems();

          $.publish('plugin/swiper/onLoadItemsSuccess', [ me, response ]);

          if (typeof callback === 'function' && me.itemsCount > 0) {
            callback.call(me, response);
          }
        }
      });

      $.publish('plugin/swiper/onLoadItems', [ me ]);
    },

    /**
     * Tracks the number of items the slider contains.
     *
     * @public
     * @method trackItems
     * @returns {Number}
     */
    trackItems: function () {
      var me = this;

      me.$items = me.$sliderWrapperElement.children();

      $.publish('plugin/slider/trackItems', me);

      return me.itemsCount = me.$items.length;
    },

    destroy: function () {
      var me = this;
      me._destroy();
    }
  });
})(jQuery);
